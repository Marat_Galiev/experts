Rails.application.routes.draw do
  require 'sidekiq/web'
  require 'sidekiq-status/web'
  mount Sidekiq::Web => '/sidekiq'
  resources :members
  post :friendships, to: 'friendships#create', path: '/members/:id/friendships'
  post :search, to: 'members#search', path: '/members/:id/search'
end