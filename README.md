# EXPERTS APP

Short description and some test notes.

## Run
Run sidekiq:
```
bundle exec sidekiq -C config/sidekiq.yml
```
Run rails server.
```
rails s
```

POST: http://localhost:3000/members
Creates expert.
Payload:

```json
{
	"member": {
		"name": "Ivan",
		"personal_website": "http://google.com"
	}
}
```

GET: http://localhost:3000/members/:ID
Get expert.
Payload:

```json
{
	"member": {
		"name": "Ivan",
		"personal_website": "http://google.com"
	}
}
```

POST: http://localhost:3000/members/:ID/friendships
Creates friendship.
Payload:

```json
{
	"member": {
		"friend_ids": [
			:FRIEND_ID
		]
	}
}
```

GET: http://localhost:3000/members
Get experts.

POST: http://localhost:3000/members/8/search
Search.
Payload:

```json
{
	"q": "Goog"
}
```

### Data structure examples

Sample data for members:
```
8	Alan	http://maratgaliev.com	https://bit.ly/3eSsPtr	2
9	Bart	http://lol.com	https://bit.ly/3dQOi4L	        5
10	Claudia	http://random.com	https://bit.ly/2Zus6rP   	2
11	Mark	http://omg.com	https://bit.ly/2Zus6rP      	2
12	Ivan	http://google.com	https://bit.ly/2VDq5Zp    	1
```

Sample data for friendships:
```
5	Member	9	8	2
6	Member	8	9   2
7	Member	9	10	2
8	Member	10	9   2
9	Member	9	11	2
10	Member	11	9	2
11	Member	11	12	2
12	Member	12	11	2
```
Sample data for headings:
```
50	h3	Google	12
```

### Results
Example search results:

http://localhost:3000/members/8/search

```json
{
  "result": [
    [
      "Alan",
      "Bart",
      "Mark",
      "Ivan (Google)"
    ]
  ]
}
```