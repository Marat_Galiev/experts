class CreateHeadings < ActiveRecord::Migration[6.0]
  def change
    create_table :headings do |t|
      t.string :tag_type
      t.text :content
      t.references :member
      t.timestamps
    end
  end
end
