class CreateMembers < ActiveRecord::Migration[6.0]
  def change
    create_table :members do |t|
      t.string :name
      t.string :personal_website
      t.string :shorten_personal_website
      t.integer :friends_count, default: 0
      t.string :parse_error
      t.timestamps
    end
  end
end
