module Search
  class Query
    include Dry::Transaction

    step :find_headings
    step :graph_scope

    def self.index_query(id:, q:, &block)
      new.call(id: id, q: q, &block)
    end

    def build_graph
      @graph = Structures::Graph.new
      data = HasFriendship::Friendship.pluck(:friendable_id, :friend_id)
      data.each do |friends|
        node = @graph.get_node(friends.first)
        friend_node = @graph.get_node(friends.last)
        
        if !node
          node = Structures::Node.new(friends.first)
        end
        if !friend_node
          friend_node = Structures::Node.new(friends.last)
        end

        @graph.nodes << node
        @graph.nodes << friend_node

        @graph.add_edge(node, friend_node)
      end
      @graph
    end

    def find_headings(id:, q:)
      member = Member.find_by(id: id)
      if member
        exclude_ids = member.friend_ids + [id]
        result_pairs = Heading.joins(:member).where.not(member_id: exclude_ids).where('LOWER(content) LIKE LOWER(?)', "%#{q}%").select(:id, 'members.name', :member_id, :content).as_json #pluck(:member_id, :content)
        Success(id: id, result_pairs: result_pairs)
      else
        Failure(I18n.t('errors.members.not_found'))
      end
    end

    def get_from_hash(hash, id, index)
      result = hash.select { |item| item["member_id"] == id }[index]&.fetch('content')
      result = " (#{result})" if result && result.present?
    end

    def graph_scope(id:, result_pairs:)
      graph = build_graph
      me = graph.get_node(id.to_i)
      results = []
      result_pairs.each_with_index do |result, index|
        friend = graph.get_node(result['member_id'].to_i)
        path = Structures::GraphQuery.new(graph, me).build_path_to(friend)
        graph_path = path.map(&:member_id)
        mems = Member.where(id: graph_path).sort_by { |p| graph_path.index(p.id) }
        data = get_from_hash(result_pairs, result['member_id'], index)
        if data
          results << mems.map { |x| "#{x.name}#{get_from_hash(result_pairs, x.id, index)}" }
        end
      end

      Success(result: results)
    end
  end
end