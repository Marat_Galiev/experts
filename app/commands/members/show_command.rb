module Members
  class ShowCommand < BaseCommand
    step :get

    def get(id:)
      member = Member.find_by(id: id)
      if member
        links = member.friends.map { |friend| "#{Rails.application.routes.url_helpers.member_path(friend)}" }
        member.friends_links = links
        Success(member)
      else
        Failure(error(I18n.t('errors.members.not_found', id: id)))
      end
    end
  end
end