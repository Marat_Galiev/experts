module Members
  class FriendshipCommand < BaseCommand
    step :persist

    def persist(member_id:, friend_ids:)
      member = Member.find_by(id: member_id)
      if member
        Members::FriendsService.new(member_id, friend_ids).call
        Success(member)
      else
        Failure(error(I18n.t('errors.members.not_found')))
      end
    end
  end
end