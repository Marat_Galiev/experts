module Members
  class CreateCommand < BaseCommand
    step :persist

    def persist(params:)
      member = Member.new(params)
      if member.save
        Success(member)
      else
        Failure(error(member.errors.full_messages.to_sentence))
      end
    end
  end
end