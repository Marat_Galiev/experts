require "set"

module Structures
  class Node
    attr_accessor :member_id, :adjacents

    def initialize(member_id)
      @adjacents = Set.new
      @member_id = member_id
    end

    def to_s
      @member_id
    end
  end
end