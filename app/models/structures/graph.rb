module Structures
  class Graph
    attr_accessor :nodes
  
    def initialize
      @nodes = []
    end
    
    def get_node(id)
      @nodes.select { |x| x.member_id == id }&.first
    end
  
    def add_edge(node_one, node_two)
      node_one.adjacents << node_two
      node_two.adjacents << node_one
    end
  end
end