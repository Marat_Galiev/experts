class Member < ApplicationRecord
  has_friendship

  attr_accessor :friends_links

  after_save :set_short_url, :parse_headers

  has_many :headings

  def friends_of_friends
    Member.joins(:friendships).where(:id => friendships.pluck(:friend_id))
  end

  def set_short_url
    Members::ShortenService.new(self.id).call
  end

  def parse_headers
    Members::TagParserService.new(self.id).call
  end

  def as_json(options={})
    options[:methods] = [:friends_links]
    super
  end
end
