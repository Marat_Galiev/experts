class MembersController < BaseController
  def index
    Members::Query.index_query(params) do |q|
      q.success {|members| array_api_response(members, MemberSerializer) }
      q.failure {|errors| error_response(errors) }
    end
  end

  def search
    Search::Query.index_query(id: params[:id], q: params[:q]) do |q|
      q.success {|members| array_api_response(members, MemberSerializer) }
      q.failure {|errors| error_response(errors) }
    end
  end

  def create
    run_command(Members::CreateCommand, params: member_params)
  end

  def show
    Members::ShowCommand.run(id: params[:id]) do |m|
      m.success {|data| api_response(data, MemberDetailsSerializer) }
      m.failure {|errors| error_response(errors, :not_found) }
    end
  end

  private

  def member_params
    params.require(:member).permit(:name, :personal_website)
  end
end