class FriendshipsController < BaseController
  
  def create
    Members::FriendshipCommand.run(member_id: params[:id], friend_ids: friendship_params[:friend_ids]) do |m|
      m.success {|member| api_response(member) }
      m.failure {|errors| error_response(errors, :not_found) }
    end
  end
  
  private

  def friendship_params
    params.require(:member).permit(:friend_ids => [])
  end
end