class BaseController < ApplicationController
  private

  def array_api_response(data, serializer)
    render json: data, each_serializer: serializer
  end

  def api_response(data, serializer = nil)
    if serializer
      render json: data, serializer: serializer
    else
      render json: data
    end
  end

  def error_response(errors, status=:unprocessable_entity)
    render json: {errors: errors}, status: status
  end

  def run_command(command, params, serializer = nil)
    command.run(params) do |m|
      m.success {|object| (serializer ? api_response(object, serializer) : api_response(object)) }
      m.failure {|errors| error_response(errors) }
    end
  end
end