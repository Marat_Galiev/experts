class MemberSerializer < ActiveModel::Serializer
  attributes :name, :shorten_personal_website, :friends_count
end