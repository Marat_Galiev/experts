class MemberDetailsSerializer < ActiveModel::Serializer
  attributes :name, :personal_website, :shorten_personal_website, :friends_links, :headings

  def headings
    object.headings.map(&:content)
  end
end