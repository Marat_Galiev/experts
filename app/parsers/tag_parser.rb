require 'open-uri'

class TagParser
  def self.parse(url)
    result = { result: [], error: nil }
    tags = ['h1', 'h3']
    
    begin
      page = open(url)
      doc = Nokogiri::HTML(page)
      tags.each do |tag|
        doc.css(tag).each do |item|
          result[:result] << { type: tag, tag: item.text }
        end
      end
    rescue Exception => e
      result[:error] = e.message
    end
    result
  end
end