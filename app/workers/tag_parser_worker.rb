class TagParserWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options retry: false
  sidekiq_options queue: 'tags'
  
  def perform(member_id)
    member = Member.find(member_id)
    website = member.personal_website
    if website
      parse_result = TagParser.parse(website)
      if parse_result[:error]
        member.update_column(:parse_error, parse_result[:error]) 
      else
        parse_result[:result].each do |t|
          Heading.create(member_id: member_id, tag_type: t[:type], content: t[:tag])
        end
      end
    end
  end
end