class FriendsWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options retry: false
  sidekiq_options queue: 'friends'
  
  def perform(member_id, friend_ids)
    member = Member.find(member_id)
    if member && friend_ids&.any?
      friends = Member.where(id: friend_ids)
      friends.each do |friend|
        member.friend_request(friend)
        unless member.friends_with?(friend)
          friend.accept_request(member)
          Member.increment_counter('friends_count', member.id)
          Member.increment_counter('friends_count', friend.id)
        end
      end
    end
  end
end