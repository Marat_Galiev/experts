class ShortenWorker
  include Sidekiq::Worker
  include Sidekiq::Status::Worker
  sidekiq_options retry: false
  sidekiq_options queue: 'shorten_urls'
  
  def perform(member_id)
    member = Member.find(member_id)
    if member.personal_website
      client = Bitly::API::Client.new(token: ENV['BIT_TOKEN'])
      link = client.shorten(long_url: member.personal_website)&.link
      member.update_column(:shorten_personal_website, link)
    end
  end
end