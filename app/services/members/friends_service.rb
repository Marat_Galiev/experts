module Members
  class FriendsService
    attr_accessor :member_id, :friends_ids

    def initialize(member_id, friends_ids)
      self.member_id = member_id
      self.friends_ids = friends_ids
    end
  
    def call
      FriendsWorker.perform_async(@member_id, @friends_ids)
    end
  end
end