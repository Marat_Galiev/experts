module Members
  class SearchService
    attr_accessor :member_id

    def initialize(member_id)
      self.member_id = member_id
    end
  
    def call
      ShortenWorker.perform_async(@member_id)
    end
  end
end