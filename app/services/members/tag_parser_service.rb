module Members
  class TagParserService
    attr_accessor :member_id

    def initialize(member_id)
      self.member_id = member_id
    end
  
    def call
      TagParserWorker.perform_async(@member_id)
    end
  end
end